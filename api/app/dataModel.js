// Categories helper
const returnCategories = filters => {
  if (filters) {
    const categoryFilter = filters.filter(filter => filter.id === 'category');
    return categoryFilter.length ? categoryFilter[0].values[0].path_from_root : [];
  }
  return [];
};

// Search Model
const searchModel = data => {
  return {
    author: {
      name: '',
      lastname: '',
    },
    categories: returnCategories(data.filters),
    items: data.results.map(item => ({
      id: item.id,
      title: item.title,
      price: {
        currency: item.currency_id,
        amount: Math.trunc(item.price),
        decimals: Number(item.price)
          .toFixed(2)
          .split('.')[1],
      },
      picture: item.thumbnail,
      condition: item.condition,
      free_shipping: item.shipping.free_shipping,
      address: {
        state_name: item.address.state_name,
      },
    })),
  };
};

// Description and Detail Model
const descriptionModel = (data, description) => {
  return {
    author: {
      name: '',
      lastname: '',
    },
    categories: returnCategories(data.filters),
    item: {
      id: data.id,
      title: data.title,
      price: {
        currency: data.currency_id,
        amount: Math.trunc(data.price),
        decimals: Number(data.price)
          .toFixed(2)
          .split('.')[1],
      },
      picture: data.pictures[0].url,
      condition: data.condition,
      free_shipping: data.shipping.free_shipping,
      sold_quantity: data.sold_quantity,
      description: description.plain_text,
    },
  };
};

module.exports = { searchModel, descriptionModel };
