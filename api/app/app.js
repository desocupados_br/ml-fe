const express = require('express');
const fetch = require('node-fetch');
const cors = require('cors');
const { searchModel, descriptionModel } = require('./dataModel');

const app = express();

app.use(cors());

app.get('/api/items', async (req, res) => {
  const response = await fetch(`https://api.mercadolibre.com/sites/MLA/search?q=${req.query.search}`);
  const data = await response.json();
  res.status(200).send({
    status: 200,
    data: await searchModel(data),
  });
});

app.get('/api/items/:id', async (req, res) => {
  const response = await fetch(`https://api.mercadolibre.com/items/${req.params.id}`);
  const responseData = await response.json();

  const description = await fetch(`https://api.mercadolibre.com/items/${req.params.id}/description`);
  const descriptionData = await description.json();

  res.status(200).send({
    status: 200,
    data: await descriptionModel(responseData, descriptionData),
  });
});

const PORT = 5000;

app.listen(PORT, () => {
  console.log(`API running on port ${PORT}`);
});
