import React, { Suspense, lazy } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Loading from './molecules/Loading/Loading';
import './App.scss';

const HomePage = lazy(() => import('./pages/HomePage/HomePage'));
const ItemsPage = lazy(() => import('./pages/ItemsPage/ItemsPage'));
const DetailsPage = lazy(() => import('./pages/DetailsPage/DetailsPage'));

const App = () => (
  <BrowserRouter>
    <Suspense fallback={<Loading />}>
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route exact path="/items" component={ItemsPage} />
        <Route exact path="/items/:id" component={DetailsPage} />
      </Switch>
    </Suspense>
  </BrowserRouter>
);

export default App;
