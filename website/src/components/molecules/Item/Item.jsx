import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import ItemPrice from '../../atoms/ItemPrice/ItemPrice';

const Item = ({ item }) => (
  <div className="item">
    <Link to={`/items/${item.id}`}>
      <div className="item-image">
        <img className="item__image" src={item.picture} alt={item.title} />
      </div>
    </Link>
    <div className="item-info">
      <ItemPrice item={item} />
      <h2 className="item__title">{item.title}</h2>
    </div>
    <address className="item__address">{item.address.state_name}</address>
  </div>
);

Item.propTypes = {
  item: PropTypes.shape({
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    price: PropTypes.shape({
      currency: PropTypes.string,
      amount: PropTypes.number,
      decimals: PropTypes.string,
    }).isRequired,
    picture: PropTypes.string.isRequired,
    condition: PropTypes.string.isRequired,
    free_shipping: PropTypes.bool.isRequired,
    address: PropTypes.shape({
      state_name: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default Item;
