import React from 'react';

const RequestError = () => {
  return (
      <span>
        Houve um problema ao tentar conectar com o backend. A API está ligada ?
      </span>
  );
};

export default RequestError;
