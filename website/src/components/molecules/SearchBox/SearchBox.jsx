import React from 'react';
import PropTypes from 'prop-types';
import SearchButton from '../../atoms/SearchButton/SearchButton';
import Input from '../../atoms/Input/Input';
import './SearchBox.scss';

const SearchBox = ({ searchQuery, onSubmit, onSearchQueryChange }) => (
  <form className="searchBox" onSubmit={onSubmit}>
    <Input
      onSearchQueryChange={onSearchQueryChange}
      type="text"
      value={searchQuery}
      className="searchBox__searchInput"
      placeholder="Nunca dejes de buscar"
    />
    <SearchButton className="button button__searchButton" type="submit" />
  </form>
);

SearchBox.propTypes = {
  searchQuery: PropTypes.string,
  onSubmit: PropTypes.func.isRequired,
  onSearchQueryChange: PropTypes.func.isRequired,
};

SearchBox.defaultProps = {
  searchQuery: '',
};

export default SearchBox;
