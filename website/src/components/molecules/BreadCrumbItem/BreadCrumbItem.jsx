import React from 'react';
import PropTypes from 'prop-types';
import RightArrow from '../../../imgs/arrow-right.svg';

const BreadCrumbItem = ({ breadCrumbsItem }) => (
  <li className="breadCrumbs__item" key={breadCrumbsItem.id}>
    <span>{breadCrumbsItem.name}</span>
    <RightArrow className="breadCrumbs__rightArrow" />
  </li>
);

BreadCrumbItem.propTypes = {
  breadCrumbsItem: PropTypes.shape({}).isRequired,
};

export default BreadCrumbItem;
