import React from 'react';
import './Loading.scss';

const Loading = () => (
  <div className="loading">
    <span>Carregando...</span>
  </div>
);

export default Loading;
