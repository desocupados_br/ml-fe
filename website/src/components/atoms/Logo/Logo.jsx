import React from 'react'
import logoIcon from '../../../imgs/Logo_ML.png'
import logoIcon2x from '../../../imgs/Logo_ML@2x.png'

const Logo = props => (
    <React.Fragment>
        <img
            {...props}
            src={logoIcon}
            srcSet={`${logoIcon2x} 2x`}
            alt="Mercado Livre"
            width="53"
            height="36"
        />
    </React.Fragment>
)

export default Logo
