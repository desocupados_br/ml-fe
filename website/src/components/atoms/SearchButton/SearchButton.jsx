import React from 'react';
import searchBtnIcon from '../../../imgs/ic_Search.png';
import searchBtnIcon2x from '../../../imgs/ic_Search@2x.png';
import './SearchButton.scss';

const SearchButton = () => (
  <React.Fragment>
    <button className="searchButton" type="submit">
      <img src={searchBtnIcon} srcSet={`${searchBtnIcon2x} 2x`} alt="Buscar" />
    </button>
  </React.Fragment>
);

export default SearchButton;
