import React from 'react';
import PropTypes from 'prop-types';
import './ItemPrice.scss';
import freeShipping from '../../../imgs/ic_shipping.png';
import freeShipping2x from '../../../imgs/ic_shipping@2x.png';

const ItemPrice = ({ item, priceStyle }) =>
  priceStyle === 'styled' ? (
    <div className="item-price--styled">
      <span className="item__price">{`$ ${Number(item.price.amount).toLocaleString('pt-BR')}`}</span>
      <span className="item__decimals">{item.price.decimals}</span>
    </div>
  ) : (
    <div className="item-price">
      <span className="item__price">
        {`$ ${Number(item.price.amount).toLocaleString('pt-BR')},${item.price.decimals}`}
      </span>
      {item.free_shipping && (
        <img className="item__freeShipping" src={freeShipping} srcSet={`${freeShipping2x} 2x`} alt="" />
      )}
    </div>
  );

ItemPrice.propTypes = {
  item: PropTypes.shape({}).isRequired,
  priceStyle: PropTypes.string,
};

ItemPrice.defaultProps = {
  priceStyle: 'normal',
};

export default ItemPrice;
