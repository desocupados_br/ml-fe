import React from 'react';
import PropTypes from 'prop-types';
import './Input.scss';

const Input = ({ onSearchQueryChange, ...props }) => <input {...props} onChange={onSearchQueryChange} type="text" />;

Input.propTypes = {
  onSearchQueryChange: PropTypes.func.isRequired,
};
export default Input;
