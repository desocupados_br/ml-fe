import React from 'react';
import Header from '../../organisms/Header/Header';

const HomePage = () => (
  <div className="app">
    <Header />
  </div>
);

export default HomePage;
