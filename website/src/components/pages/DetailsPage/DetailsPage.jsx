import React, { Component } from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import Header from '../../organisms/Header/Header';
import BreadCrumbs from '../../organisms/BreadCrumbs/BreadCrumbs';
import ItemPrice from '../../atoms/ItemPrice/ItemPrice';
import Loading from '../../molecules/Loading/Loading';
import RequestError from '../../molecules/RequestError/RequestError';
import './DetailsPage.scss';

class DetailsPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      itemDetails: {
        author: {
          name: '',
          lastname: '',
        },
        categories: [],
        item: {
          id: '',
          title: '',
          price: {
            currency: '',
            amount: '',
            decimals: '',
          },
          picture: '',
          condition: '',
          free_shipping: '',
          sold_quantity: '',
          description: '',
        },
      },
    };
  }

  componentDidMount() {
    this.setState({ loading: true, requestError: false });
    const { match } = this.props;
    axios.get(`http://localhost:5000/api/items/${match.params.id}`).then((response) => {
      this.setState({ itemDetails: response.data.data, loading: false, requestError: false });
    }).catch((error) => {
      console.error(`Houve um erro ao acessar a API. Erro: ${error}`);
      this.setState({
        loading: false,
        requestError: true
      })
    });
  }

  render() {
    const { itemDetails, loading, requestError } = this.state;
    const { picture, title, description, condition } = itemDetails.item;
    //  linter force to write on camelCase
    const soldQuantity = itemDetails.item.sold_quantity;
    return (
      <div className="app">
        <Header handleListFetching={this.handleListFetching} />
        {itemDetails.categories.length > 0 && <BreadCrumbs categories={itemDetails.categories} />}
        {loading && !requestError ?  <Loading /> : (
          <section className="content-wrapper details">
            <div className="details-innerWrapper">
              <div className="details-content">
                <img className="details-content__image" src={picture} alt={title} />
                <div className="details-content-description">
                  <h2 className="description__title">Descripción del producto</h2>
                  <p className="description__content">{description}</p>
                </div>
              </div>
              <div className="details-info">
                <div className="condition-sold">
                  <span className="details__condition">{condition === 'new' ? 'Novo' : 'Usado'} - </span>
                  <span className="details__sold">
                    {soldQuantity > 0 ? `${soldQuantity} vendidos` : 'nenhum vendido'}
                  </span>
                </div>
                <h1 className="details__title">{title}</h1>
                <ItemPrice priceStyle="styled" item={itemDetails.item} />
                <div role="button" className="btn btn__buy">
                  Comprar
                </div>
              </div>
            </div>
          </section>
        )}
        {requestError && <RequestError />}
      </div>
    );
  }
}

DetailsPage.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({}).isRequired,
  }).isRequired,
};

export default DetailsPage;
