import React, { Component } from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import Header from '../../organisms/Header/Header';
import ItemsList from '../../organisms/ItemsList/ItemsList';
import BreadCrumbs from '../../organisms/BreadCrumbs/BreadCrumbs';
import Loading from '../../molecules/Loading/Loading';
import RequestError from '../../molecules/RequestError/RequestError';

class ItemsPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      itemsList: [],
      categories: [],
      loading: false,
      requestError: false
    };
  }

  handleListFetching = (searchQuery) => {
    this.setState({ loading: true, requestError: false });
    axios
      .get(`http://localhost:5000/api/items?search=${encodeURIComponent(searchQuery)}`)
      .then((response) => {
        this.setState({ itemsList: response.data.data.items.slice(0, 4) });
        this.setState({ categories: response.data.data.categories });
        this.setState({ loading: false });
      })
      .catch((error) => {
        console.error(`Houve um erro ao acessar a API. Erro: ${error}`);
        this.setState({
          loading: false,
          requestError: true
        })
      });
  };

  render() {
    const { itemsList, categories, loading, requestError } = this.state;
    return (
      <div className="app">
        <Header handleListFetching={this.handleListFetching} />
        {categories.length > 0 && !loading && <BreadCrumbs categories={categories} />}
        {loading && !requestError ? <Loading /> : <ItemsList itemsList={itemsList} />}
        {requestError && <RequestError />}
      </div>
    );
  }
}

ItemsPage.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired,
  }).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func.isRequired,
  }).isRequired,
};

export default ItemsPage;
