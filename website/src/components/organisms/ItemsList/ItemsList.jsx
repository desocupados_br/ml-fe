import React from 'react';
import PropTypes from 'prop-types';
import Item from '../../molecules/Item/Item';
import './ItemsList.scss';

const ItemsList = ({ itemsList }) => (
  <section className="content-wrapper results">
    <div className="results-innerWrapper">
      {itemsList.length ? (
        <ul className="result-list">
          {itemsList.map((item) => (
            <li key={item.id} className="result-item">
              <Item key={item.id} item={item} />
            </li>
          ))}
        </ul>
      ) : (
        <div>Não foi encontrado nenhum produto =(</div>
      )}
    </div>
  </section>
);

ItemsList.propTypes = {
  itemsList: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
      price: PropTypes.shape({
        currency: PropTypes.string,
        amount: PropTypes.number,
        decimals: PropTypes.string,
      }).isRequired,
      picture: PropTypes.string.isRequired,
      condition: PropTypes.string.isRequired,
      free_shipping: PropTypes.bool.isRequired,
    })
  ).isRequired,
};

export default ItemsList;
