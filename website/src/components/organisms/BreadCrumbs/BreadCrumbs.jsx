import React from 'react';
import PropTypes from 'prop-types';
import BreadCrumbsItem from '../../molecules/BreadCrumbItem/BreadCrumbItem';
import './BreadCrumbs.scss';

const BreadCrumbs = ({ categories }) => (
  <section className="breadCrumbs">
    <div className="breadCrumbs-innerWrapper">
      <ul className="breadCrumbs-nav">
        {categories.map((category) => (
          <BreadCrumbsItem key={category.id} breadCrumbsItem={category} />
        ))}
      </ul>
    </div>
  </section>
);

BreadCrumbs.propTypes = {
  categories: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
    })
  ).isRequired,
};
export default BreadCrumbs;
