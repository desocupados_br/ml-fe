import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import SearchBox from '../../molecules/SearchBox/SearchBox';
import Logo from '../../atoms/Logo/Logo';
import './Header.scss';

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchQuery: '',
    };
  }

  componentDidMount() {
    const { location } = this.props;
    const params = new URLSearchParams(location.search);
    const searchQueryParam = params.get('search');
    if (searchQueryParam) {
      this.setState({ searchQuery: searchQueryParam });
      this.fetchItems(searchQueryParam);
    }
  }

  handleSearchQueryChange = (event) => {
    this.setState({ searchQuery: event.target.value });
  };

  handleSubmit = (event) => {
    event.preventDefault();
    const { history } = this.props;
    const { searchQuery } = this.state;
    if (searchQuery) {
      history.push({ pathname: '/items', search: `?search=${searchQuery}` });

      this.fetchItems(searchQuery);
    }
  };

  fetchItems = (searchQuery) => {
    const { handleListFetching } = this.props;
    handleListFetching(searchQuery);
  };

  render() {
    const { searchQuery } = this.state;
    return (
      <header className="header">
        <div className="header__innerWrapper">
          <Link to="/">
            <Logo className="header__logo" />
          </Link>
          <SearchBox
            searchQuery={searchQuery}
            onSearchQueryChange={this.handleSearchQueryChange}
            onSubmit={this.handleSubmit}
          />
        </div>
      </header>
    );
  }
}

Header.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired,
  }).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func.isRequired,
  }).isRequired,
  handleListFetching: PropTypes.func,
};

Header.defaultProps = {
  handleListFetching: () => '',
};

export default withRouter(Header);
