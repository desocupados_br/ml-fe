# Teste para Analista Front End Pleno

## FRONTEND (/website)
## Env

node version: >= 9.11.2

## Install

`npm install`

## Run in dev mode

`npm run dev`

## Build for production

`npm run build`

## Run tests

`npm run test`

## Access

http://localhost:8080

--

## BACKEND (/api)

### Env

node version: >= 9.11.2

express version: 4.16.4

### Install

`npm install`

### Run

`npm run start`

### Access

http://localhost:5000

--

#Features

* Dev environment (with source maps, hot module reload)
* Prod environment (build)
* Testing environment
* Atomic architecture
* Code Splitting
* Lazy Loading
* Sass support
* Responsive Layout
* Responsive Images (based on client screen resolution @2x)
* PropTypes check
* ES6+ (using babel)
* ESLint (AirB'nB styleguide)
* Prettier
* Network error handling